EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x40_Odd_Even J101
U 1 1 5F801EF1
P 1850 3700
F 0 "J101" H 1900 5817 50  0000 C CNN
F 1 "GPIO" H 1900 5726 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x40_P2.54mm_Vertical" H 1850 3700 50  0001 C CNN
F 3 "~" H 1850 3700 50  0001 C CNN
	1    1850 3700
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR0101
U 1 1 5F80E506
P 650 5700
F 0 "#PWR0101" H 650 5550 50  0001 C CNN
F 1 "+3V3" H 665 5873 50  0000 C CNN
F 2 "" H 650 5700 50  0001 C CNN
F 3 "" H 650 5700 50  0001 C CNN
	1    650  5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	650  5700 650  5800
Wire Wire Line
	650  5800 1650 5800
Wire Wire Line
	2200 5800 2200 5700
Wire Wire Line
	2200 5600 2150 5600
Wire Wire Line
	2150 5700 2200 5700
Connection ~ 2200 5700
Wire Wire Line
	2200 5700 2200 5600
Wire Wire Line
	1650 5600 1650 5700
Wire Wire Line
	1650 5700 1650 5800
Connection ~ 1650 5700
Connection ~ 1650 5800
Wire Wire Line
	1650 5800 2200 5800
$Comp
L power:+12V #PWR0103
U 1 1 5F81818E
P 1600 1400
F 0 "#PWR0103" H 1600 1250 50  0001 C CNN
F 1 "+12V" H 1615 1573 50  0000 C CNN
F 2 "" H 1600 1400 50  0001 C CNN
F 3 "" H 1600 1400 50  0001 C CNN
	1    1600 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 1900 1600 1900
Wire Wire Line
	1600 1900 1600 1800
Wire Wire Line
	1650 1800 1600 1800
Connection ~ 1600 1800
Wire Wire Line
	1600 1800 1600 1500
Wire Wire Line
	2150 1900 2200 1900
Wire Wire Line
	2200 1900 2200 1800
Wire Wire Line
	2200 1500 1600 1500
Connection ~ 1600 1500
Wire Wire Line
	1600 1500 1600 1400
Wire Wire Line
	2150 1800 2200 1800
Connection ~ 2200 1800
Wire Wire Line
	2200 1800 2200 1500
$Comp
L power:GND #PWR0102
U 1 1 5F87C6FB
P 1350 5850
F 0 "#PWR0102" H 1350 5600 50  0001 C CNN
F 1 "GND" H 1355 5677 50  0000 C CNN
F 2 "" H 1350 5850 50  0001 C CNN
F 3 "" H 1350 5850 50  0001 C CNN
	1    1350 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 2000 1350 2000
Wire Wire Line
	1350 2000 1350 2100
Wire Wire Line
	1650 2100 1350 2100
Connection ~ 1350 2100
$Comp
L power:GND #PWR0104
U 1 1 5F881075
P 2450 5850
F 0 "#PWR0104" H 2450 5600 50  0001 C CNN
F 1 "GND" H 2455 5677 50  0000 C CNN
F 2 "" H 2450 5850 50  0001 C CNN
F 3 "" H 2450 5850 50  0001 C CNN
	1    2450 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 2100 2450 2100
Wire Wire Line
	2150 2000 2450 2000
Wire Wire Line
	2450 2000 2450 2100
Connection ~ 2450 2100
Wire Wire Line
	2150 5500 2450 5500
Connection ~ 2450 5500
Wire Wire Line
	2450 5500 2450 5850
Wire Wire Line
	2150 5400 2450 5400
Connection ~ 2450 5400
Wire Wire Line
	2450 5400 2450 5500
Wire Wire Line
	1650 5400 1350 5400
Connection ~ 1350 5400
Wire Wire Line
	1350 5400 1350 5500
Wire Wire Line
	1650 5500 1350 5500
Connection ~ 1350 5500
Wire Wire Line
	1350 5500 1350 5850
Wire Wire Line
	2450 2100 2450 2200
Wire Wire Line
	2150 2200 2450 2200
Connection ~ 2450 2200
Wire Wire Line
	2450 2200 2450 3100
Wire Wire Line
	2150 3100 2450 3100
Connection ~ 2450 3100
Wire Wire Line
	2450 3100 2450 3200
Wire Wire Line
	2150 3200 2450 3200
Connection ~ 2450 3200
Wire Wire Line
	2450 3200 2450 3300
Wire Wire Line
	2150 3300 2450 3300
Connection ~ 2450 3300
Wire Wire Line
	2450 3300 2450 4200
Wire Wire Line
	1650 3100 1350 3100
Connection ~ 1350 3100
Wire Wire Line
	1350 3100 1350 3200
Wire Wire Line
	1650 3200 1350 3200
Connection ~ 1350 3200
Wire Wire Line
	1350 3200 1350 3300
Wire Wire Line
	1650 3300 1350 3300
Connection ~ 1350 3300
Wire Wire Line
	1350 3300 1350 4200
Wire Wire Line
	2150 4200 2450 4200
Connection ~ 2450 4200
Wire Wire Line
	2450 4200 2450 4300
Wire Wire Line
	2150 4300 2450 4300
Connection ~ 2450 4300
Wire Wire Line
	2450 4300 2450 4400
Wire Wire Line
	2150 4400 2450 4400
Connection ~ 2450 4400
Wire Wire Line
	2450 4400 2450 5300
Wire Wire Line
	2150 5300 2450 5300
Connection ~ 2450 5300
Wire Wire Line
	2450 5300 2450 5400
Wire Wire Line
	1650 4200 1350 4200
Connection ~ 1350 4200
Wire Wire Line
	1350 4200 1350 4300
Wire Wire Line
	1650 4300 1350 4300
Connection ~ 1350 4300
Wire Wire Line
	1350 4300 1350 4400
Wire Wire Line
	1650 4400 1350 4400
Connection ~ 1350 4400
Wire Wire Line
	1350 4400 1350 5300
Wire Wire Line
	1650 5300 1350 5300
Connection ~ 1350 5300
Wire Wire Line
	1350 5300 1350 5400
$Comp
L Connector_Generic:Conn_02x40_Odd_Even J102
U 1 1 5F956279
P 10050 3400
F 0 "J102" H 10100 5517 50  0000 C CNN
F 1 "GPIO" H 10100 5426 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x40_P2.54mm_Vertical" H 10050 3400 50  0001 C CNN
F 3 "~" H 10050 3400 50  0001 C CNN
	1    10050 3400
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR0107
U 1 1 5F95627F
P 8850 5400
F 0 "#PWR0107" H 8850 5250 50  0001 C CNN
F 1 "+3V3" H 8865 5573 50  0000 C CNN
F 2 "" H 8850 5400 50  0001 C CNN
F 3 "" H 8850 5400 50  0001 C CNN
	1    8850 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	8850 5400 8850 5500
Wire Wire Line
	8850 5500 9850 5500
Wire Wire Line
	10400 5500 10400 5400
Wire Wire Line
	10400 5300 10350 5300
Wire Wire Line
	10350 5400 10400 5400
Connection ~ 10400 5400
Wire Wire Line
	10400 5400 10400 5300
Wire Wire Line
	9850 5300 9850 5400
Wire Wire Line
	9850 5400 9850 5500
Connection ~ 9850 5400
Connection ~ 9850 5500
Wire Wire Line
	9850 5500 10400 5500
$Comp
L power:+12V #PWR0109
U 1 1 5F956291
P 9800 1100
F 0 "#PWR0109" H 9800 950 50  0001 C CNN
F 1 "+12V" H 9815 1273 50  0000 C CNN
F 2 "" H 9800 1100 50  0001 C CNN
F 3 "" H 9800 1100 50  0001 C CNN
	1    9800 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	9850 1600 9800 1600
Wire Wire Line
	9800 1600 9800 1500
Wire Wire Line
	9850 1500 9800 1500
Connection ~ 9800 1500
Wire Wire Line
	9800 1500 9800 1200
Wire Wire Line
	10350 1600 10400 1600
Wire Wire Line
	10400 1600 10400 1500
Wire Wire Line
	10400 1200 9800 1200
Connection ~ 9800 1200
Wire Wire Line
	9800 1200 9800 1100
Wire Wire Line
	10350 1500 10400 1500
Connection ~ 10400 1500
Wire Wire Line
	10400 1500 10400 1200
Text Label 9850 1800 2    50   ~ 0
PWM0
Text Label 9850 1900 2    50   ~ 0
PWM2
Text Label 9850 2100 2    50   ~ 0
ADC0
Text Label 9850 2400 2    50   ~ 0
I2C0_SDA
Text Label 9850 2500 2    50   ~ 0
I2C1_SDA
Text Label 9850 2800 2    50   ~ 0
SPI0_SCLK
Text Label 9850 2700 2    50   ~ 0
SPI0_MOSI
Text Label 9850 2200 2    50   ~ 0
ADC2
Text Label 10350 1800 0    50   ~ 0
PWM1
Text Label 10350 1900 0    50   ~ 0
PWM3
Text Label 10350 2100 0    50   ~ 0
ADC1
Text Label 10350 2200 0    50   ~ 0
ADC3
Text Label 10350 2400 0    50   ~ 0
I2C0_SCL
Text Label 10350 2500 0    50   ~ 0
I2C1_SCL
Text Label 10350 2700 0    50   ~ 0
SPI0_MISO
Text Label 10350 2800 0    50   ~ 0
SPI0_SS
$Comp
L power:GND #PWR0108
U 1 1 5F9562BE
P 9400 5550
F 0 "#PWR0108" H 9400 5300 50  0001 C CNN
F 1 "GND" H 9405 5377 50  0000 C CNN
F 2 "" H 9400 5550 50  0001 C CNN
F 3 "" H 9400 5550 50  0001 C CNN
	1    9400 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	9850 1700 9400 1700
$Comp
L power:GND #PWR0110
U 1 1 5F9562C8
P 10800 5550
F 0 "#PWR0110" H 10800 5300 50  0001 C CNN
F 1 "GND" H 10805 5377 50  0000 C CNN
F 2 "" H 10800 5550 50  0001 C CNN
F 3 "" H 10800 5550 50  0001 C CNN
	1    10800 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	10350 1700 10800 1700
Wire Wire Line
	9850 5200 9400 5200
Connection ~ 9400 5200
Wire Wire Line
	9400 5200 9400 5550
Wire Wire Line
	10800 1700 10800 2000
Wire Wire Line
	9400 1700 9400 2000
Wire Wire Line
	10350 5200 10800 5200
Connection ~ 10800 5200
Wire Wire Line
	10800 5200 10800 5550
Text Label 10350 3000 0    50   ~ 0
SPI1_MISO
Text Label 10350 3100 0    50   ~ 0
SPI1_SS
Text Label 9850 3100 2    50   ~ 0
SPI1_SCLK
Text Label 9850 3000 2    50   ~ 0
SPI1_MOSI
Wire Wire Line
	10350 2000 10800 2000
Connection ~ 10800 2000
Wire Wire Line
	10800 2000 10800 2300
Wire Wire Line
	10350 2300 10800 2300
Connection ~ 10800 2300
Wire Wire Line
	10800 2300 10800 2600
Wire Wire Line
	9850 2000 9400 2000
Connection ~ 9400 2000
Wire Wire Line
	9400 2000 9400 2300
Wire Wire Line
	9850 2300 9400 2300
Connection ~ 9400 2300
Wire Wire Line
	9400 2300 9400 2600
Wire Wire Line
	9850 2600 9400 2600
Connection ~ 9400 2600
Wire Wire Line
	9400 2600 9400 2900
Wire Wire Line
	10350 2600 10800 2600
Connection ~ 10800 2600
Wire Wire Line
	10800 2600 10800 2900
Wire Wire Line
	10350 2900 10800 2900
Connection ~ 10800 2900
Wire Wire Line
	10800 2900 10800 3200
Wire Wire Line
	9850 2900 9400 2900
Connection ~ 9400 2900
Wire Wire Line
	9400 2900 9400 3200
Wire Wire Line
	9850 3200 9400 3200
Connection ~ 9400 3200
Wire Wire Line
	9400 3200 9400 5200
Wire Wire Line
	10350 3200 10800 3200
Connection ~ 10800 3200
Wire Wire Line
	10800 3200 10800 5200
$Comp
L YAAJ_BlackPill:YAAJ_BlackPill U101
U 1 1 5FA05C7C
P 5700 2850
F 0 "U101" H 5700 4031 50  0000 C CNN
F 1 "YAAJ_BlackPill" H 5700 3940 50  0000 C CNN
F 2 "Modules:YAAJ_BlackPill_SWD_Breakout_1" V 6350 2000 50  0001 C CNN
F 3 "" V 6350 2000 50  0001 C CNN
	1    5700 2850
	1    0    0    -1  
$EndComp
Entry Wire Line
	6650 3250 6750 3150
Entry Wire Line
	6650 3150 6750 3050
Entry Wire Line
	6650 3050 6750 2950
Entry Wire Line
	6650 2950 6750 2850
Entry Wire Line
	6650 2850 6750 2750
Entry Wire Line
	6650 2750 6750 2650
Entry Wire Line
	6650 2650 6750 2550
Entry Wire Line
	6750 2450 6650 2550
Entry Wire Line
	2600 3000 2700 2900
Entry Wire Line
	2600 2900 2700 2800
Entry Wire Line
	2600 2800 2700 2700
Entry Wire Line
	2600 2700 2700 2600
Entry Wire Line
	2600 2600 2700 2500
Entry Wire Line
	2600 2500 2700 2400
Entry Wire Line
	2600 2400 2700 2300
Entry Wire Line
	2700 2200 2600 2300
Wire Wire Line
	2150 2300 2200 2300
Entry Wire Line
	1100 2200 1200 2300
Entry Wire Line
	1100 2300 1200 2400
Entry Wire Line
	1100 2400 1200 2500
Entry Wire Line
	1100 2500 1200 2600
Entry Wire Line
	1100 2600 1200 2700
Entry Wire Line
	1100 2700 1200 2800
Entry Wire Line
	1100 2800 1200 2900
Entry Wire Line
	1200 3000 1100 2900
$Comp
L Device:Jumper_NC_Small JP125
U 1 1 5FB4CE9C
P 2300 2300
F 0 "JP125" H 2300 2512 50  0001 C CNN
F 1 "Jumper_NC_Small" H 2300 2421 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2300 2300 50  0001 C CNN
F 3 "~" H 2300 2300 50  0001 C CNN
	1    2300 2300
	1    0    0    -1  
$EndComp
Entry Wire Line
	4650 2550 4750 2650
Entry Wire Line
	4650 2650 4750 2750
Entry Wire Line
	4650 2750 4750 2850
Entry Wire Line
	4650 2850 4750 2950
Entry Wire Line
	4750 3050 4650 2950
Entry Wire Line
	4650 2450 4750 2550
Wire Bus Line
	1100 1150 2700 1150
Wire Wire Line
	2150 2400 2200 2400
$Comp
L Device:Jumper_NC_Small JP126
U 1 1 5FCAE7B7
P 2300 2400
F 0 "JP126" H 2300 2612 50  0001 C CNN
F 1 "Jumper_NC_Small" H 2300 2521 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2300 2400 50  0001 C CNN
F 3 "~" H 2300 2400 50  0001 C CNN
	1    2300 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 2500 2200 2500
$Comp
L Device:Jumper_NC_Small JP127
U 1 1 5FCB6C56
P 2300 2500
F 0 "JP127" H 2300 2712 50  0001 C CNN
F 1 "Jumper_NC_Small" H 2300 2621 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2300 2500 50  0001 C CNN
F 3 "~" H 2300 2500 50  0001 C CNN
	1    2300 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 2600 2200 2600
$Comp
L Device:Jumper_NC_Small JP128
U 1 1 5FCB6C5E
P 2300 2600
F 0 "JP128" H 2300 2812 50  0001 C CNN
F 1 "Jumper_NC_Small" H 2300 2721 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2300 2600 50  0001 C CNN
F 3 "~" H 2300 2600 50  0001 C CNN
	1    2300 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 2700 2200 2700
$Comp
L Device:Jumper_NC_Small JP129
U 1 1 5FCC120E
P 2300 2700
F 0 "JP129" H 2300 2912 50  0001 C CNN
F 1 "Jumper_NC_Small" H 2300 2821 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2300 2700 50  0001 C CNN
F 3 "~" H 2300 2700 50  0001 C CNN
	1    2300 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 2800 2200 2800
$Comp
L Device:Jumper_NC_Small JP130
U 1 1 5FCC1216
P 2300 2800
F 0 "JP130" H 2300 3012 50  0001 C CNN
F 1 "Jumper_NC_Small" H 2300 2921 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2300 2800 50  0001 C CNN
F 3 "~" H 2300 2800 50  0001 C CNN
	1    2300 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 2900 2200 2900
$Comp
L Device:Jumper_NC_Small JP131
U 1 1 5FCC121E
P 2300 2900
F 0 "JP131" H 2300 3112 50  0001 C CNN
F 1 "Jumper_NC_Small" H 2300 3021 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2300 2900 50  0001 C CNN
F 3 "~" H 2300 2900 50  0001 C CNN
	1    2300 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 3000 2200 3000
$Comp
L Device:Jumper_NC_Small JP132
U 1 1 5FCC1226
P 2300 3000
F 0 "JP132" H 2300 3212 50  0001 C CNN
F 1 "Jumper_NC_Small" H 2300 3121 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2300 3000 50  0001 C CNN
F 3 "~" H 2300 3000 50  0001 C CNN
	1    2300 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 3400 2200 3400
$Comp
L Device:Jumper_NC_Small JP133
U 1 1 5FDA5963
P 2300 3400
F 0 "JP133" H 2300 3612 50  0001 C CNN
F 1 "Jumper_NC_Small" H 2300 3521 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2300 3400 50  0001 C CNN
F 3 "~" H 2300 3400 50  0001 C CNN
	1    2300 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 3500 2200 3500
$Comp
L Device:Jumper_NC_Small JP134
U 1 1 5FDA596A
P 2300 3500
F 0 "JP134" H 2300 3712 50  0001 C CNN
F 1 "Jumper_NC_Small" H 2300 3621 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2300 3500 50  0001 C CNN
F 3 "~" H 2300 3500 50  0001 C CNN
	1    2300 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 3600 2200 3600
$Comp
L Device:Jumper_NC_Small JP135
U 1 1 5FDA5971
P 2300 3600
F 0 "JP135" H 2300 3812 50  0001 C CNN
F 1 "Jumper_NC_Small" H 2300 3721 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2300 3600 50  0001 C CNN
F 3 "~" H 2300 3600 50  0001 C CNN
	1    2300 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 3700 2200 3700
$Comp
L Device:Jumper_NC_Small JP136
U 1 1 5FDA5978
P 2300 3700
F 0 "JP136" H 2300 3912 50  0001 C CNN
F 1 "Jumper_NC_Small" H 2300 3821 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2300 3700 50  0001 C CNN
F 3 "~" H 2300 3700 50  0001 C CNN
	1    2300 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 3800 2200 3800
$Comp
L Device:Jumper_NC_Small JP137
U 1 1 5FDA597F
P 2300 3800
F 0 "JP137" H 2300 4012 50  0001 C CNN
F 1 "Jumper_NC_Small" H 2300 3921 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2300 3800 50  0001 C CNN
F 3 "~" H 2300 3800 50  0001 C CNN
	1    2300 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 3900 2200 3900
$Comp
L Device:Jumper_NC_Small JP138
U 1 1 5FDA5986
P 2300 3900
F 0 "JP138" H 2300 4112 50  0001 C CNN
F 1 "Jumper_NC_Small" H 2300 4021 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2300 3900 50  0001 C CNN
F 3 "~" H 2300 3900 50  0001 C CNN
	1    2300 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 4000 2200 4000
$Comp
L Device:Jumper_NC_Small JP139
U 1 1 5FDA598D
P 2300 4000
F 0 "JP139" H 2300 4212 50  0001 C CNN
F 1 "Jumper_NC_Small" H 2300 4121 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2300 4000 50  0001 C CNN
F 3 "~" H 2300 4000 50  0001 C CNN
	1    2300 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 4100 2200 4100
$Comp
L Device:Jumper_NC_Small JP140
U 1 1 5FDA5994
P 2300 4100
F 0 "JP140" H 2300 4312 50  0001 C CNN
F 1 "Jumper_NC_Small" H 2300 4221 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2300 4100 50  0001 C CNN
F 3 "~" H 2300 4100 50  0001 C CNN
	1    2300 4100
	1    0    0    -1  
$EndComp
Text Label 1200 2300 0    50   ~ 0
A0
Text Label 2600 2300 2    50   ~ 0
A1
Text Label 1200 2400 0    50   ~ 0
A2
Text Label 2600 2400 2    50   ~ 0
A3
Text Label 1200 2500 0    50   ~ 0
A4
Text Label 2600 2500 2    50   ~ 0
A5
Text Label 1200 2600 0    50   ~ 0
A6
Text Label 2600 2600 2    50   ~ 0
A7
Wire Wire Line
	1350 2100 1350 2200
Wire Wire Line
	1650 2300 1600 2300
$Comp
L Device:Jumper_NC_Small JP101
U 1 1 5FF1F90B
P 1500 2300
F 0 "JP101" H 1500 2512 50  0001 C CNN
F 1 "Jumper_NC_Small" H 1500 2421 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1500 2300 50  0001 C CNN
F 3 "~" H 1500 2300 50  0001 C CNN
	1    1500 2300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1650 2400 1600 2400
$Comp
L Device:Jumper_NC_Small JP102
U 1 1 5FF1F912
P 1500 2400
F 0 "JP102" H 1500 2612 50  0001 C CNN
F 1 "Jumper_NC_Small" H 1500 2521 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1500 2400 50  0001 C CNN
F 3 "~" H 1500 2400 50  0001 C CNN
	1    1500 2400
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1650 2500 1600 2500
$Comp
L Device:Jumper_NC_Small JP103
U 1 1 5FF1F919
P 1500 2500
F 0 "JP103" H 1500 2712 50  0001 C CNN
F 1 "Jumper_NC_Small" H 1500 2621 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1500 2500 50  0001 C CNN
F 3 "~" H 1500 2500 50  0001 C CNN
	1    1500 2500
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1650 2600 1600 2600
$Comp
L Device:Jumper_NC_Small JP104
U 1 1 5FF1F920
P 1500 2600
F 0 "JP104" H 1500 2812 50  0001 C CNN
F 1 "Jumper_NC_Small" H 1500 2721 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1500 2600 50  0001 C CNN
F 3 "~" H 1500 2600 50  0001 C CNN
	1    1500 2600
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1650 2700 1600 2700
$Comp
L Device:Jumper_NC_Small JP105
U 1 1 5FF1F927
P 1500 2700
F 0 "JP105" H 1500 2912 50  0001 C CNN
F 1 "Jumper_NC_Small" H 1500 2821 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1500 2700 50  0001 C CNN
F 3 "~" H 1500 2700 50  0001 C CNN
	1    1500 2700
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1650 2800 1600 2800
$Comp
L Device:Jumper_NC_Small JP106
U 1 1 5FF1F92E
P 1500 2800
F 0 "JP106" H 1500 3012 50  0001 C CNN
F 1 "Jumper_NC_Small" H 1500 2921 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1500 2800 50  0001 C CNN
F 3 "~" H 1500 2800 50  0001 C CNN
	1    1500 2800
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1650 2900 1600 2900
$Comp
L Device:Jumper_NC_Small JP107
U 1 1 5FF1F935
P 1500 2900
F 0 "JP107" H 1500 3112 50  0001 C CNN
F 1 "Jumper_NC_Small" H 1500 3021 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1500 2900 50  0001 C CNN
F 3 "~" H 1500 2900 50  0001 C CNN
	1    1500 2900
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1650 3000 1600 3000
$Comp
L Device:Jumper_NC_Small JP108
U 1 1 5FF1F93C
P 1500 3000
F 0 "JP108" H 1500 3212 50  0001 C CNN
F 1 "Jumper_NC_Small" H 1500 3121 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1500 3000 50  0001 C CNN
F 3 "~" H 1500 3000 50  0001 C CNN
	1    1500 3000
	-1   0    0    -1  
$EndComp
Text Label 2600 2700 2    50   ~ 0
A9
Text Label 1200 2800 0    50   ~ 0
A10
Text Label 2600 2800 2    50   ~ 0
A11
Text Label 1200 2900 0    50   ~ 0
A12
Text Label 2600 2900 2    50   ~ 0
A13
Text Label 1200 3000 0    50   ~ 0
A14
Text Label 2600 3000 2    50   ~ 0
A15
Text Label 1200 2700 0    50   ~ 0
A8
Wire Wire Line
	1200 2300 1400 2300
Wire Wire Line
	1200 2400 1400 2400
Wire Wire Line
	1200 2500 1400 2500
Wire Wire Line
	1200 2600 1400 2600
Wire Wire Line
	1200 2700 1400 2700
Wire Wire Line
	1200 2800 1400 2800
Wire Wire Line
	1200 2900 1400 2900
Wire Wire Line
	1200 3000 1400 3000
Text Label 1100 1150 0    50   ~ 0
A[0..15]
Text Label 4750 2550 0    50   ~ 0
A8
Text Label 4750 2650 0    50   ~ 0
A9
Text Label 4750 2750 0    50   ~ 0
A10
Text Label 4750 2850 0    50   ~ 0
A11
Text Label 4750 2950 0    50   ~ 0
A12
Text Label 4750 3050 0    50   ~ 0
A15
Connection ~ 4650 1150
Wire Wire Line
	4750 2550 4900 2550
Wire Wire Line
	4750 2650 4900 2650
Wire Wire Line
	4750 2750 4900 2750
Wire Wire Line
	4750 2850 4900 2850
Wire Wire Line
	4750 2950 4900 2950
Wire Wire Line
	4750 3050 4900 3050
Text Label 6650 3250 2    50   ~ 0
A0
Text Label 6650 3150 2    50   ~ 0
A1
Text Label 6650 3050 2    50   ~ 0
A2
Text Label 6650 2950 2    50   ~ 0
A3
Text Label 6650 2850 2    50   ~ 0
A4
Text Label 6650 2750 2    50   ~ 0
A5
Text Label 6650 2650 2    50   ~ 0
A6
Text Label 6650 2550 2    50   ~ 0
A7
Wire Bus Line
	6750 1150 4650 1150
Wire Wire Line
	6500 2550 6650 2550
Wire Wire Line
	6500 2650 6650 2650
Wire Wire Line
	6500 2750 6650 2750
Wire Wire Line
	6500 2850 6650 2850
Wire Wire Line
	6500 2950 6650 2950
Wire Wire Line
	6500 3050 6650 3050
Wire Wire Line
	6500 3150 6650 3150
Wire Wire Line
	6500 3250 6650 3250
Connection ~ 2700 1150
Wire Bus Line
	2700 1150 4650 1150
Wire Wire Line
	2400 2300 2600 2300
Wire Wire Line
	2400 2400 2600 2400
Wire Wire Line
	2400 2500 2600 2500
Wire Wire Line
	2400 2600 2600 2600
Wire Wire Line
	2400 2700 2600 2700
Wire Wire Line
	2400 2800 2600 2800
Wire Wire Line
	2400 2900 2600 2900
Wire Wire Line
	2400 3000 2600 3000
Wire Wire Line
	1650 2200 1350 2200
Connection ~ 1350 2200
Wire Wire Line
	1350 2200 1350 3100
Entry Wire Line
	4450 2250 4350 2150
Entry Wire Line
	4450 2150 4350 2050
Entry Wire Line
	4450 2450 4350 2350
Entry Wire Line
	4450 2350 4350 2250
Entry Wire Line
	4450 3250 4350 3150
Entry Wire Line
	4450 3150 4350 3050
Entry Wire Line
	4450 3450 4350 3350
Entry Wire Line
	4450 3350 4350 3250
Entry Wire Line
	4450 3550 4350 3450
Text Label 4450 2150 0    50   ~ 0
B12
Text Label 4450 2250 0    50   ~ 0
B13
Text Label 4450 2350 0    50   ~ 0
B14
Text Label 4450 2450 0    50   ~ 0
B15
Wire Wire Line
	4450 2150 4900 2150
Wire Wire Line
	4450 2250 4900 2250
Wire Wire Line
	4450 2350 4900 2350
Text Label 4450 3150 0    50   ~ 0
B3
Text Label 4450 3250 0    50   ~ 0
B4
Text Label 4450 3350 0    50   ~ 0
B5
Text Label 4450 3450 0    50   ~ 0
B6
Text Label 4450 3550 0    50   ~ 0
B7
Wire Wire Line
	4450 3150 4900 3150
Wire Wire Line
	4450 3250 4900 3250
Wire Wire Line
	4450 3350 4900 3350
Wire Wire Line
	4450 3450 4900 3450
Wire Wire Line
	4450 3550 4900 3550
Text Label 1200 3400 0    50   ~ 0
B0
Text Label 2600 3400 2    50   ~ 0
B1
Text Label 1200 3500 0    50   ~ 0
B2
Text Label 2600 3500 2    50   ~ 0
B3
Text Label 1200 3600 0    50   ~ 0
B4
Text Label 2600 3600 2    50   ~ 0
B5
Text Label 1200 3700 0    50   ~ 0
B6
Text Label 2600 3700 2    50   ~ 0
B7
Text Label 1200 3800 0    50   ~ 0
B8
Text Label 2600 3800 2    50   ~ 0
B9
Text Label 1200 3900 0    50   ~ 0
B10
Text Label 2600 3900 2    50   ~ 0
B11
Text Label 1200 4000 0    50   ~ 0
B12
Text Label 2600 4000 2    50   ~ 0
B13
Text Label 1200 4100 0    50   ~ 0
B14
Text Label 2600 4100 2    50   ~ 0
B15
Wire Wire Line
	1650 3400 1600 3400
$Comp
L Device:Jumper_NC_Small JP109
U 1 1 606E24E4
P 1500 3400
F 0 "JP109" H 1500 3612 50  0001 C CNN
F 1 "Jumper_NC_Small" H 1500 3521 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1500 3400 50  0001 C CNN
F 3 "~" H 1500 3400 50  0001 C CNN
	1    1500 3400
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1650 3500 1600 3500
$Comp
L Device:Jumper_NC_Small JP110
U 1 1 606E24EB
P 1500 3500
F 0 "JP110" H 1500 3712 50  0001 C CNN
F 1 "Jumper_NC_Small" H 1500 3621 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1500 3500 50  0001 C CNN
F 3 "~" H 1500 3500 50  0001 C CNN
	1    1500 3500
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1650 3600 1600 3600
$Comp
L Device:Jumper_NC_Small JP111
U 1 1 606E24F2
P 1500 3600
F 0 "JP111" H 1500 3812 50  0001 C CNN
F 1 "Jumper_NC_Small" H 1500 3721 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1500 3600 50  0001 C CNN
F 3 "~" H 1500 3600 50  0001 C CNN
	1    1500 3600
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1650 3700 1600 3700
$Comp
L Device:Jumper_NC_Small JP112
U 1 1 606E24F9
P 1500 3700
F 0 "JP112" H 1500 3912 50  0001 C CNN
F 1 "Jumper_NC_Small" H 1500 3821 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1500 3700 50  0001 C CNN
F 3 "~" H 1500 3700 50  0001 C CNN
	1    1500 3700
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1650 3800 1600 3800
$Comp
L Device:Jumper_NC_Small JP113
U 1 1 606E2500
P 1500 3800
F 0 "JP113" H 1500 4012 50  0001 C CNN
F 1 "Jumper_NC_Small" H 1500 3921 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1500 3800 50  0001 C CNN
F 3 "~" H 1500 3800 50  0001 C CNN
	1    1500 3800
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1650 3900 1600 3900
$Comp
L Device:Jumper_NC_Small JP114
U 1 1 606E2507
P 1500 3900
F 0 "JP114" H 1500 4112 50  0001 C CNN
F 1 "Jumper_NC_Small" H 1500 4021 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1500 3900 50  0001 C CNN
F 3 "~" H 1500 3900 50  0001 C CNN
	1    1500 3900
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1650 4000 1600 4000
$Comp
L Device:Jumper_NC_Small JP115
U 1 1 606E250E
P 1500 4000
F 0 "JP115" H 1500 4212 50  0001 C CNN
F 1 "Jumper_NC_Small" H 1500 4121 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1500 4000 50  0001 C CNN
F 3 "~" H 1500 4000 50  0001 C CNN
	1    1500 4000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1650 4100 1600 4100
$Comp
L Device:Jumper_NC_Small JP116
U 1 1 606E2515
P 1500 4100
F 0 "JP116" H 1500 4312 50  0001 C CNN
F 1 "Jumper_NC_Small" H 1500 4221 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1500 4100 50  0001 C CNN
F 3 "~" H 1500 4100 50  0001 C CNN
	1    1500 4100
	-1   0    0    -1  
$EndComp
Entry Wire Line
	1100 3300 1200 3400
Entry Wire Line
	1100 3400 1200 3500
Entry Wire Line
	1100 3500 1200 3600
Entry Wire Line
	1100 3600 1200 3700
Entry Wire Line
	1100 3700 1200 3800
Entry Wire Line
	1100 3800 1200 3900
Entry Wire Line
	1100 3900 1200 4000
Entry Wire Line
	1100 4000 1200 4100
Wire Wire Line
	1200 3400 1400 3400
Wire Wire Line
	1200 3500 1400 3500
Wire Wire Line
	1200 3600 1400 3600
Wire Wire Line
	1200 3700 1400 3700
Wire Wire Line
	1200 3800 1400 3800
Wire Wire Line
	1200 3900 1400 3900
Wire Wire Line
	1200 4000 1400 4000
Wire Wire Line
	1200 4100 1400 4100
Wire Wire Line
	2400 3400 2600 3400
Wire Wire Line
	2400 3500 2600 3500
Wire Wire Line
	2400 3600 2600 3600
Wire Wire Line
	2400 3700 2600 3700
Wire Wire Line
	2400 3800 2600 3800
Wire Wire Line
	2400 3900 2600 3900
Wire Wire Line
	2400 4000 2600 4000
Wire Wire Line
	2400 4100 2600 4100
Entry Wire Line
	2700 3300 2600 3400
Entry Wire Line
	2700 3400 2600 3500
Entry Wire Line
	2700 3500 2600 3600
Entry Wire Line
	2700 3600 2600 3700
Entry Wire Line
	2700 3700 2600 3800
Entry Wire Line
	2700 3800 2600 3900
Entry Wire Line
	2700 3900 2600 4000
Entry Wire Line
	2700 4000 2600 4100
Wire Bus Line
	1100 3300 1000 3300
Wire Bus Line
	1000 3300 1000 1050
Wire Bus Line
	1000 1050 2800 1050
Wire Bus Line
	2700 3300 2800 3300
Wire Bus Line
	2800 3300 2800 1050
Connection ~ 2800 1050
Wire Bus Line
	2800 1050 4350 1050
Text Label 1000 1050 0    50   ~ 0
B[0..15]
Text Label 6900 2150 2    50   ~ 0
B11
Text Label 6900 2250 2    50   ~ 0
B10
Text Label 6900 2350 2    50   ~ 0
B1
Text Label 6900 2450 2    50   ~ 0
B0
Entry Wire Line
	6900 2450 7000 2350
Entry Wire Line
	6900 2350 7000 2250
Entry Wire Line
	6900 2250 7000 2150
Entry Wire Line
	7000 2050 6900 2150
Wire Wire Line
	6500 2150 6900 2150
Wire Wire Line
	6500 2250 6900 2250
Wire Wire Line
	6500 2350 6900 2350
Text Label 6900 3550 2    50   ~ 0
B9
Text Label 6900 3650 2    50   ~ 0
B8
Entry Wire Line
	6900 3650 7000 3550
Entry Wire Line
	7000 3450 6900 3550
Wire Wire Line
	6500 3550 6900 3550
Wire Wire Line
	6500 3650 6900 3650
Wire Bus Line
	7000 1050 4350 1050
Connection ~ 4350 1050
$Comp
L power:+3V3 #PWR0105
U 1 1 60A60DBD
P 5700 1600
F 0 "#PWR0105" H 5700 1450 50  0001 C CNN
F 1 "+3V3" H 5715 1773 50  0000 C CNN
F 2 "" H 5700 1600 50  0001 C CNN
F 3 "" H 5700 1600 50  0001 C CNN
	1    5700 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 1600 5700 1850
Wire Wire Line
	5600 3950 5800 3950
$Comp
L power:GND #PWR0106
U 1 1 60A87301
P 5800 3950
F 0 "#PWR0106" H 5800 3700 50  0001 C CNN
F 1 "GND" H 5805 3777 50  0000 C CNN
F 2 "" H 5800 3950 50  0001 C CNN
F 3 "" H 5800 3950 50  0001 C CNN
	1    5800 3950
	1    0    0    -1  
$EndComp
Connection ~ 5800 3950
Wire Wire Line
	2150 4500 2200 4500
$Comp
L Device:Jumper_NC_Small JP141
U 1 1 60BF1A4B
P 2300 4500
F 0 "JP141" H 2300 4712 50  0001 C CNN
F 1 "Jumper_NC_Small" H 2300 4621 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2300 4500 50  0001 C CNN
F 3 "~" H 2300 4500 50  0001 C CNN
	1    2300 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 4600 2200 4600
$Comp
L Device:Jumper_NC_Small JP142
U 1 1 60BF1A52
P 2300 4600
F 0 "JP142" H 2300 4812 50  0001 C CNN
F 1 "Jumper_NC_Small" H 2300 4721 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2300 4600 50  0001 C CNN
F 3 "~" H 2300 4600 50  0001 C CNN
	1    2300 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 4700 2200 4700
$Comp
L Device:Jumper_NC_Small JP143
U 1 1 60BF1A59
P 2300 4700
F 0 "JP143" H 2300 4912 50  0001 C CNN
F 1 "Jumper_NC_Small" H 2300 4821 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2300 4700 50  0001 C CNN
F 3 "~" H 2300 4700 50  0001 C CNN
	1    2300 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 4800 2200 4800
$Comp
L Device:Jumper_NC_Small JP144
U 1 1 60BF1A60
P 2300 4800
F 0 "JP144" H 2300 5012 50  0001 C CNN
F 1 "Jumper_NC_Small" H 2300 4921 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2300 4800 50  0001 C CNN
F 3 "~" H 2300 4800 50  0001 C CNN
	1    2300 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 4900 2200 4900
$Comp
L Device:Jumper_NC_Small JP145
U 1 1 60BF1A67
P 2300 4900
F 0 "JP145" H 2300 5112 50  0001 C CNN
F 1 "Jumper_NC_Small" H 2300 5021 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2300 4900 50  0001 C CNN
F 3 "~" H 2300 4900 50  0001 C CNN
	1    2300 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 5000 2200 5000
$Comp
L Device:Jumper_NC_Small JP146
U 1 1 60BF1A6E
P 2300 5000
F 0 "JP146" H 2300 5212 50  0001 C CNN
F 1 "Jumper_NC_Small" H 2300 5121 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2300 5000 50  0001 C CNN
F 3 "~" H 2300 5000 50  0001 C CNN
	1    2300 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 5100 2200 5100
$Comp
L Device:Jumper_NC_Small JP147
U 1 1 60BF1A75
P 2300 5100
F 0 "JP147" H 2300 5312 50  0001 C CNN
F 1 "Jumper_NC_Small" H 2300 5221 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2300 5100 50  0001 C CNN
F 3 "~" H 2300 5100 50  0001 C CNN
	1    2300 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 5200 2200 5200
$Comp
L Device:Jumper_NC_Small JP148
U 1 1 60BF1A7C
P 2300 5200
F 0 "JP148" H 2300 5412 50  0001 C CNN
F 1 "Jumper_NC_Small" H 2300 5321 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2300 5200 50  0001 C CNN
F 3 "~" H 2300 5200 50  0001 C CNN
	1    2300 5200
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper_NC_Small JP117
U 1 1 60C0B466
P 1500 4500
F 0 "JP117" H 1500 4712 50  0001 C CNN
F 1 "Jumper_NC_Small" H 1500 4621 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1500 4500 50  0001 C CNN
F 3 "~" H 1500 4500 50  0001 C CNN
	1    1500 4500
	-1   0    0    -1  
$EndComp
$Comp
L Device:Jumper_NC_Small JP118
U 1 1 60C0B46C
P 1500 4600
F 0 "JP118" H 1500 4812 50  0001 C CNN
F 1 "Jumper_NC_Small" H 1500 4721 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1500 4600 50  0001 C CNN
F 3 "~" H 1500 4600 50  0001 C CNN
	1    1500 4600
	-1   0    0    -1  
$EndComp
$Comp
L Device:Jumper_NC_Small JP119
U 1 1 60C0B472
P 1500 4700
F 0 "JP119" H 1500 4912 50  0001 C CNN
F 1 "Jumper_NC_Small" H 1500 4821 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1500 4700 50  0001 C CNN
F 3 "~" H 1500 4700 50  0001 C CNN
	1    1500 4700
	-1   0    0    -1  
$EndComp
$Comp
L Device:Jumper_NC_Small JP120
U 1 1 60C0B478
P 1500 4800
F 0 "JP120" H 1500 5012 50  0001 C CNN
F 1 "Jumper_NC_Small" H 1500 4921 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1500 4800 50  0001 C CNN
F 3 "~" H 1500 4800 50  0001 C CNN
	1    1500 4800
	-1   0    0    -1  
$EndComp
$Comp
L Device:Jumper_NC_Small JP121
U 1 1 60C0B47E
P 1500 4900
F 0 "JP121" H 1500 5112 50  0001 C CNN
F 1 "Jumper_NC_Small" H 1500 5021 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1500 4900 50  0001 C CNN
F 3 "~" H 1500 4900 50  0001 C CNN
	1    1500 4900
	-1   0    0    -1  
$EndComp
$Comp
L Device:Jumper_NC_Small JP122
U 1 1 60C0B484
P 1500 5000
F 0 "JP122" H 1500 5212 50  0001 C CNN
F 1 "Jumper_NC_Small" H 1500 5121 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1500 5000 50  0001 C CNN
F 3 "~" H 1500 5000 50  0001 C CNN
	1    1500 5000
	-1   0    0    -1  
$EndComp
$Comp
L Device:Jumper_NC_Small JP123
U 1 1 60C0B48A
P 1500 5100
F 0 "JP123" H 1500 5312 50  0001 C CNN
F 1 "Jumper_NC_Small" H 1500 5221 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1500 5100 50  0001 C CNN
F 3 "~" H 1500 5100 50  0001 C CNN
	1    1500 5100
	-1   0    0    -1  
$EndComp
$Comp
L Device:Jumper_NC_Small JP124
U 1 1 60C0B490
P 1500 5200
F 0 "JP124" H 1500 5412 50  0001 C CNN
F 1 "Jumper_NC_Small" H 1500 5321 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1500 5200 50  0001 C CNN
F 3 "~" H 1500 5200 50  0001 C CNN
	1    1500 5200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1600 4500 1650 4500
Wire Wire Line
	1600 4600 1650 4600
Wire Wire Line
	1600 4700 1650 4700
Wire Wire Line
	1600 4800 1650 4800
Wire Wire Line
	1600 4900 1650 4900
Wire Wire Line
	1600 5000 1650 5000
Wire Wire Line
	1600 5100 1650 5100
Wire Wire Line
	1600 5200 1650 5200
Text Label 1200 4500 0    50   ~ 0
C0
Text Label 2600 4500 2    50   ~ 0
C1
Text Label 1200 4600 0    50   ~ 0
C2
Text Label 2600 4600 2    50   ~ 0
C3
Text Label 1200 4700 0    50   ~ 0
C4
Text Label 2600 4700 2    50   ~ 0
C5
Text Label 1200 4800 0    50   ~ 0
C6
Text Label 2600 4800 2    50   ~ 0
C7
Text Label 1200 4900 0    50   ~ 0
C8
Text Label 2600 4900 2    50   ~ 0
C9
Text Label 1200 5000 0    50   ~ 0
C10
Text Label 2600 5000 2    50   ~ 0
C11
Text Label 1200 5100 0    50   ~ 0
C12
Text Label 2600 5100 2    50   ~ 0
C13
Text Label 1200 5200 0    50   ~ 0
C14
Text Label 2600 5200 2    50   ~ 0
C15
Wire Wire Line
	2400 4500 2600 4500
Wire Wire Line
	2400 4600 2600 4600
Wire Wire Line
	2400 4700 2600 4700
Wire Wire Line
	2400 4800 2600 4800
Wire Wire Line
	2400 4900 2600 4900
Wire Wire Line
	2400 5000 2600 5000
Wire Wire Line
	2400 5200 2600 5200
Wire Wire Line
	1200 4500 1400 4500
Wire Wire Line
	1200 4600 1400 4600
Wire Wire Line
	1200 4700 1400 4700
Wire Wire Line
	1200 4800 1400 4800
Wire Wire Line
	1200 4900 1400 4900
Wire Wire Line
	1200 5000 1400 5000
Wire Wire Line
	1200 5100 1400 5100
Wire Wire Line
	1200 5200 1400 5200
NoConn ~ 1200 4500
NoConn ~ 1200 4600
NoConn ~ 1200 4700
NoConn ~ 1200 4800
NoConn ~ 1200 4900
NoConn ~ 1200 5000
NoConn ~ 1200 5100
NoConn ~ 1200 5200
NoConn ~ 2600 4500
NoConn ~ 2600 4600
NoConn ~ 2600 4700
NoConn ~ 2600 4800
NoConn ~ 2600 4900
NoConn ~ 2600 5000
NoConn ~ 2600 5200
Wire Wire Line
	6500 3450 6550 3450
Wire Wire Line
	6550 3450 6550 5100
Wire Wire Line
	2400 5100 6550 5100
Wire Wire Line
	4450 2450 4900 2450
Wire Wire Line
	6500 2450 6900 2450
Wire Bus Line
	4650 1150 4650 2950
Wire Bus Line
	1100 3300 1100 4000
Wire Bus Line
	2700 3300 2700 4000
Wire Bus Line
	7000 1050 7000 3550
Wire Bus Line
	4350 1050 4350 3450
Wire Bus Line
	1100 1150 1100 2900
Wire Bus Line
	2700 1150 2700 2900
Wire Bus Line
	6750 1150 6750 3150
$EndSCHEMATC
